<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Inicio extends BaseController
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model("Template_model");
		$this->load->model("Recursos_model");
		$this->load->model("Temas_model");
	}

	public function index()
	{
		$headerInfo = [
			"headers" => $this->Template_model->getHeaders("inicio"),
		];


		$data = [
			"temas" => $this->Temas_model->getTemas()
		];

		$this->loadViews("inicio/index", $headerInfo, $data);
	}
}
