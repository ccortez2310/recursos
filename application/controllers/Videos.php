<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Videos extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Template_model");
		$this->load->model("Recursos_model");
	}

	public function ver($slug = null)
	{

		$headerInfo = [
			"headers" => $this->Template_model->getHeaders($slug),
		];

		$video = $this->Recursos_model->getVideo($slug);

		$data = [
			"video" => $video,
			"relatedvideos" => $this->Recursos_model->getRelatedVideos($slug, $video->id_categoria)
		];

		$this->loadViews("videos/detalle", $headerInfo, $data);
	}
}
