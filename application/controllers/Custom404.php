<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Custom404 extends BaseController
{
	public function index()
	{
		$this->output->set_status_header('404');
		$this->loadViews('error404');
	}
}
