<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Contacto extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Template_model");
	}

	public function index()
	{
		$headerInfo = [
			"headers" => $this->Template_model->getHeaders("contacto"),
		];

		$this->loadViews("contacto/contacto", $headerInfo, null);
	}
}
