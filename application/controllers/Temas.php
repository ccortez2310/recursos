<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Temas extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Template_model");
		$this->load->model("Recursos_model");
		$this->load->model("Temas_model");
	}

	public function ver($slug = null)
	{

		$tema = $this->Temas_model->getTema($slug);

		if(!empty($tema)) {
			$headerInfo = [
				"headers" => $this->Template_model->getHeaders($slug),
			];
			$data = [
				"videos" => $this->Recursos_model->getVideos($tema->id),
				"podcasts" => $this->Recursos_model->getPodcasts($tema->id),
				"tema" => $tema
			];
			$this->loadViews("temas/tema", $headerInfo, $data);
		} else {
			$this->output->set_status_header('404');
			$this->loadViews('error404');
		}


	}
}
