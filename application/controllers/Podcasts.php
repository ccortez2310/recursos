<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'libraries/BaseController.php';

class Podcasts extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Template_model");
		$this->load->model("Recursos_model");
	}

	public function ver($slug = null)
	{

		$headerInfo = [
			"headers" => $this->Template_model->getHeaders($slug),
		];

		$podcast= $this->Recursos_model->getPodcast($slug);

		$data = [
			"podcast" => $podcast,
			"relatedpodcasts" => $this->Recursos_model->getRelatedPodcasts($slug, $podcast->id_categoria)
		];

		$this->loadViews("podcasts/podcast", $headerInfo, $data);
	}
}
