<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['seo_title'] = 'Portal de Recursos de Cuidado del Trauma';
$config['seo_desc'] = 'Portal Web donde encontraras contenido que ayuda a resolver los problemas del trauma';
$config['seo_imgurl'] =  server_url()."public/img/template/portada.png";
