<?php


if (!function_exists('fecha_letras')) {

	function fecha_letras($fecha)
	{
		$num = date("j", strtotime($fecha));
		$anno = date("Y", strtotime($fecha));
		$mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
		$mes = $mes[(date('m', strtotime($fecha)) * 1) - 1];
		return  $num . ' de ' . $mes . ' del ' . $anno;
	}
}



