<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Recursos_model extends CI_Model
{
	public function getVideos($id)
	{
		$this->db->where("estado", 1);
		$this->db->where("id_categoria", $id);
		return $this->db->get("videos")->result();
	}

	public function getVideo($slug)
	{
		$this->db->where("slug", $slug);
		$this->db->where("estado", 1);
		return $this->db->get("videos")->row();
	}

	public function getRelatedVideos($slug, $id_cat)
	{
		$this->db->where("slug !=", $slug);
		$this->db->where("estado", 1);
		$this->db->where("id_categoria", $id_cat);
		return $this->db->get("videos")->result();
	}

	public function getPodcasts($id)
	{
		$this->db->where("estado", 1);
		$this->db->where("id_categoria", $id);
		return $this->db->get("podcasts")->result();
	}

	public function getPodcast($slug)
	{
		$this->db->where("slug", $slug);
		$this->db->where("estado", 1);
		return $this->db->get("podcasts")->row();
	}

	public function getRelatedPodcasts($slug, $id_cat)
	{
		$this->db->where("slug !=", $slug);
		$this->db->where("estado", 1);
		$this->db->where("id_categoria", $id_cat);
		return $this->db->get("podcasts")->result();
	}

}
