<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Template_model extends CI_Model
{

	public function template()
	{
		$this->db->where('id', 1);
		return $this->db->get("plantilla")->row();
	}

	public function getHeaders($url)
	{
		$this->db->where('ruta', $url);
		return $this->db->get("cabeceras")->row();
	}

}
