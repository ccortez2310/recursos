<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Temas_model extends CI_Model
{
	public function getTemas()
	{
		$this->db->where("estado", 1);
		return $this->db->get("categorias")->result();
	}

	public function getTema($slug)
	{
		$this->db->where("slug", $slug);
		$this->db->where("estado", 1);
		return $this->db->get("categorias")->row();
	}
}
