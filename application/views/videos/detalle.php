<main class="mt-5 pt-5">
	<div class="container-fluid">

		<!--Section: Post-->
		<section class="mt-4">

			<!--Grid row-->
			<div class="row">

				<!--Grid column-->
				<div class="col-md-8 mb-4">

					<!--Card-->
					<div class="card mb-4 wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">

						<!--Card content-->
						<div class="card-body">

							<p class="h3 my-4"><?php echo $video->titulo ?></p>

							<p><?php echo $video->descripcion ?></p>

							<video id="video<?php echo $video->id ?>" class="video-js vjs-fluid" controls
								   preload="auto" poster="<?= server_url() . $video->poster ?>"
								   data-setup='{"playbackRates": [1, 1.5, 2] }'>
								<source src="<?= server_url() . $video->path ?>"
										type="video/mp4">
							</video>

							<div class="row py-1">
								<div class="col-md-2 col-sm-10 my-1">
									<span class="badge btn-grey">
										<i class="fas fa-volume-up" aria-hidden="true"></i>&nbsp;
										<?php echo $video->total_vistas ?>&nbsp; Reproducciones
									</span>
								</div>
								<div class="col-md-3 col-sm-10 my-1">
									<span class="badge btn-grey">
										Publicado el <?php echo fecha_letras($video->fecha) ?>
									</span>
								</div>
								<div class="col-md-6 col-sm-10  my-1">
									<a href="<?= server_url() . $video->path ?>" download
									   class="badge btn-grey waves-effect waves-light">Descargar
										<i class="fas fa-download fa-lg pl-1"></i>
									</a>
								</div>
							</div>

							<hr class="mb-2">

							<p class="h6">Si te ha gustado el video compártelo</p>

							<div class="social">
								<a type="button" style="color: #1475E0" data-share="facebook">
									<i class="fab fa-facebook-square fa-3x"></i>
								</a>
								<a type="button" class="ml-md-1" style="color: #00A6FF" data-share="twitter">
									<i class="fab fa-twitter-square fa-3x"></i>
								</a>
							</div>

						</div>

					</div>
					<!--/.Card-->

				</div>
				<!--Grid column-->

				<!--Grid column-->
				<div class="col-md-4 mb-4">
					<!--Card-->
					<div class="card mb-4 wow fadeIn animated" style="visibility: visible; animation-name: fadeIn;">

						<div class="card-header">Videos Relacionados</div>

						<!--Card content-->
						<div class="card-body">

							<ul class="list-unstyled">
								<?php foreach ($relatedvideos as $related): ?>
									<li class="media py-2">
										<div class="view overlay">
											<img class="d-flex"
												 src="<?= server_url() . $related->poster ?>"
												 alt="<?php echo $related->titulo ?>" width="168" height="94">
											<a href="<?= site_url() . 'videos/' . $related->slug ?>"
											   class="playWrapper2">

											</a>
										</div>
										<div class="media-body ml-3">
											<a href="<?= site_url() . 'videos/' . $related->slug ?>" class="text-dark">
												<h6 class="mt-0 mb-1 font-weight-bold"><?php echo $related->titulo ?></h6>
											</a>
											<span class="badge btn-grey">
												<i class="fas fa-eye" aria-hidden="true"></i>&nbsp;
												<?php echo $related->total_vistas ?>&nbsp; Vistas
											</span>
										</div>
									</li>
								<?php endforeach; ?>
							</ul>

						</div>

					</div>
					<!--/.Card-->

				</div>
				<!--Grid column-->

			</div>
			<!--Grid row-->

		</section>
		<!--Section: Post-->

	</div>
</main>

<script type="text/javascript">

	const player = videojs('video<?php echo $video->id ?>', {
		controls: true,
		posterImage: true,
		textTrackDisplay: false,
		loadingSpinner: false,
		controlBar: {
			fullscreenToggle: true,
			progressControl: true,
			remainingTimeDisplay: true
		}
	});

</script>
