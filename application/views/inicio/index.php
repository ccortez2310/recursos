<div id="carousel-example-1z" class="carousel slide carousel-fade">

	<!--Indicators-->
	<ol class="carousel-indicators">
		<li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-1z" data-slide-to="1"></li>
		<li data-target="#carousel-example-1z" data-slide-to="2"></li>
	</ol>
	<!--/.Indicators-->

	<!--Slides-->
	<div class="carousel-inner" role="listbox">

		<!--First slide-->
		<div class="carousel-item active">
			<div class="view">

				<!--Video source-->
				<video class="video-intro" autoplay="" loop="" muted="">
					<source src="https://mdbootstrap.com/img/video/animation-intro.mp4" type="video/mp4">
				</video>

				<!-- Mask & flexbox options-->
				<div class="mask rgba-black-light d-flex justify-content-center align-items-center">

					<!-- Content -->
					<div class="text-center white-text mx-5 wow fadeIn"
						 style="visibility: visible; animation-name: fadeIn;">
						<h1 class="mb-4">
							<strong>Portal de Recursos del Cuidado del Trauma</strong>
						</h1>

						<p class="mb-4 d-none d-md-block">
							<strong>Encuentra temas de interés que te ayudarán a afrontar situaciones
								de la vida real.</strong>
						</p>

						<a href="Javascript::void(0)"
						   class="btn btn-outline-white waves-effect waves-light">
							<i class="fas fa-book mr-2"></i>&nbsp;Biblioteca
						</a>
					</div>
					<!-- Content -->

				</div>
				<!-- Mask & flexbox options-->

			</div>
		</div>
		<!--/First slide-->

		<!--Second slide-->
		<div class="carousel-item">
			<div class="view"
				 style="background-image: url('https://mdbootstrap.com/img/Photos/Others/images/77.jpg'); background-repeat: no-repeat; background-size: cover;">

				<!-- Mask & flexbox options-->
				<div class="mask rgba-black-light d-flex justify-content-center align-items-center">

					<!-- Content -->
					<div class="text-center white-text mx-5 wow fadeIn"
						 style="visibility: visible; animation-name: fadeIn;">
						<h1 class="mb-4">
							<strong>Portal de Recursos del Cuidado del Trauma</strong>
						</h1>

						<p class="mb-4 d-none d-md-block">
							<strong>Encuentra temas de interés que te ayudarán a afrontar situaciones
								de la vida real.</strong>
						</p>

						<a href="Javascript::void(0)"
						   class="btn btn-outline-white waves-effect waves-light">
							<i class="fas fa-book mr-2"></i>&nbsp;Biblioteca
						</a>
					</div>
					<!-- Content -->

				</div>
				<!-- Mask & flexbox options-->
			</div>
		</div>
		<!--/Second slide-->

		<!--Third slide-->
		<div class="carousel-item">
			<div class="view">

				<!--Video source-->
				<video class="video-intro" autoplay="" loop="" muted="">
					<source src="https://mdbootstrap.com/img/video/forest.mp4" type="video/mp4">
				</video>

				<!-- Mask & flexbox options-->
				<div class="mask rgba-black-light d-flex justify-content-center align-items-center">

					<!-- Content -->
					<div class="text-center white-text mx-5 wow fadeIn"
						 style="visibility: visible; animation-name: fadeIn;">
						<h1 class="mb-4">
							<strong>Portal de Recursos del Cuidado del Trauma</strong>
						</h1>

						<p class="mb-4 d-none d-md-block">
							<strong>Encuentra temas de interés que te ayudarán a afrontar situaciones
								de la vida real.</strong>
						</p>

						<a href="Javascript::void(0)"
						   class="btn btn-outline-white waves-effect waves-light">
							<i class="fas fa-book mr-2"></i>&nbsp;Biblioteca
						</a>
					</div>
					<!-- Content -->
				</div>
				<!-- Mask & flexbox options-->
			</div>
		</div>
		<!--/Third slide-->

	</div>
	<!--/.Slides-->

	<!--Controls-->
	<a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
	<a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	</a>
	<!--/.Controls-->
</div>

<main>
	<div class="container">



		<section class="pt-5">

			<!-- Heading & Description -->
			<div class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
				<!--Section heading-->
				<h2 class="h1 text-center mb-5">Temas de Interés</h2>
				<!--Section description-->
				<p class="text-center mb-5 pb-5">En este portal encontrarás recursos educativos
					sobre temas de la vida cotidiana de los cuales en muchas ocasiones no sabemos como
					responder ante las diferentes situaciones incomodas que nos causan problemas psicologicos
					y no sé que quise decir</p>
			</div>
			<!-- Heading & Description -->

			<!--Grid row-->

			<?php foreach ($temas as $tema): ?>

				<div class="row mt-3 wow fadeIn " style="visibility: visible; animation-name: fadeIn;">

					<!--Grid column-->
					<div class="col-lg-5 col-xl-4 mb-4">
						<!--Featured image-->
						<div class="view overlay rounded z-depth-1">
							<img src="<?= server_url() . $tema->portada ?>" class="img-fluid" alt="">
							<a href="<?= base_url() . 'temas/' . $tema->slug ?>">
								<div class="mask rgba-white-slight waves-effect waves-light"></div>
							</a>
						</div>
					</div>
					<!--Grid column-->

					<!--Grid column-->
					<div class="col-lg-7 col-xl-7 ml-xl-4 col- mb-4 ">
						<h3 class="mb-3 font-weight-bold dark-grey-text">
							<strong><?php echo $tema->categoria ?></strong>
						</h3>
						<p class="grey-text"><?php echo $tema->descripcion ?></p>
						<a href="<?= base_url() . 'temas/' . $tema->slug ?>"
						   class="btn btn-primary btn-md waves-effect waves-light">Ir a los Recursos
							<i class="fas fa-play ml-2"></i>
						</a>
					</div>
					<!--Grid column-->
				</div>
				<!--Grid row-->

				<hr class="mb-5">
			<?php endforeach; ?>
		</section>
	</div>
</main>

