
<?php if (empty($videos) && empty($podcasts)) { ?>
<main class="container d-flex justify-content-center mx-auto h-100 align-items-center">
	<!--Main layout-->

	<div class="mt-5 pt-5">

		<!-- First row -->
		<div class="row">
			<div class="col-md-12 text-center float-md-none mx-auto">
				<h1 class="error404 wow fadeIn" data-wow-delay="0.2s" style="font-weight: 500; visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.2s;">
					¡Oops!
				</h1>
			</div>
		</div>
		<!-- /.First row -->

		<!-- Second row -->
		<div class="row mt-3">
			<div class="col-md-12 text-center mb-5">
				<h2 class="h2-responsive wow fadeIn mb-4" data-wow-delay="0.2s" style="font-weight: 500; visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.2s;">
					Todavia no hemos agregado recursos en esta sección
				</h2>
				<p class="wow fadeIn" data-wow-delay="0.4s" style="font-size: 1.25rem; visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.4s;">
					Haz click en el siguiente botón para volver al Inicio
				</p>
				<a href="<?= site_url()?>" class="btn btn-blue waves-effect waves-light">
					<i class="fas fa-home"></i>
				</a>

			</div>
		</div>
		<!-- /.Second row -->

	</div>

	<!--/.Main layout-->
</main>
<?php } else { ?>
<main class="mt-5 pt-5">
	<div class="container mb-5">

		<section class="pt-5 text-center">
			<div class="wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
				<!--Section heading-->
				<h2 class="h1 text-center mb-3"><?php echo $tema->categoria ?></h2>
				<!--Section description-->
				<p class="text-center"><?php echo $tema->descripcion ?></p>
			</div>
		</section>

		<?php if (!empty($videos)): ?>
			<section class="pt-5 text-center">

				<h2 class="h3 text-center mb-5">Videos</h2>
				<!--Grid row-->
				<div class="row mb-4 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

					<?php foreach ($videos as $value): ?>
						<!--Grid column-->
						<div class="col-lg-4 col-md-6 mb-4">

							<!--Card-->
							<div class="card">

								<!--Card image-->
								<div class="view overlay">
									<img src="<?= server_url() . $value->poster ?>"
										 class="card-img-top" alt="">
									<a href="<?= site_url() . 'videos/' . $value->slug ?>" class="playWrapper">

									</a>
								</div>

								<!--Card content-->
								<div class="card-body">
									<!--Title-->
									<a href="<?= site_url() . 'videos/' . $value->slug ?>" class="text-dark">
										<h4 class="card-title"><?php echo $value->titulo ?></h4>
									</a>
									<!--Text-->
									<p class="card-text"><?php echo substr($value->descripcion, 0, 255) ?>...</p>

								</div>

							</div>
							<!--/.Card-->

						</div>
						<!--Grid column-->
					<?php endforeach; ?>
				</div>
				<!--Grid row-->
			</section>
		<?php endif; ?>

		<?php if (!empty($podcasts)): ?>
			<section class="pb-5 text-center">

				<h2 class="h3 text-center mb-5 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">
					Podcasts</h2>

				<div class="row mb-4 wow fadeIn" style="visibility: visible; animation-name: fadeIn;">

					<?php foreach ($podcasts as $value): ?>

						<div class="col-lg-4 col-md-6 mb-4">
							<div class="card">
								<div class="row no-gutters">
									<div class="col-5 view overlay">
										<img src="<?= server_url() . $value->poster ?>" class="card-img"  alt="">
										<a href="<?= site_url() . 'podcasts/' . $value->slug ?>"
										   class="playWrapper2">

										</a>
									</div>
									<div class="col-7">
										<div class="card-block card-body px-2">
											<!--Title-->
											<a href="<?= site_url() . 'podcasts/' . $value->slug ?>" class="text-dark">
												<h6 class="card-title font-weight-bold"><?php echo $value->titulo ?></h6>
											</a>
										</div>
									</div>
								</div>
							</div>

						</div>

					<?php endforeach; ?>
				</div>
				<!--Grid row-->
			</section>
		<?php endif; ?>
	</div>
</main>
<?php } ?>
