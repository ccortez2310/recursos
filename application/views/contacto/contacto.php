<section class="container-fluid jumbotron primary-color-dark text-white text-center my-5">
	<h1 class="text-uppercase py-5">Contáctanos</h1>
</section>

<div class="container-fluid">

	<div class="container">

		<div class="row py-5">

			<div class="col-md-5 col-sm-12 pb-5">

				<div class="card">
					<div class="card-body">
						<form class="text-center">

							<p class="h4">Contáctanos</p>
							<br>
							<input type="text" id="defaultContactFormName" class="form-control mb-4"
								   placeholder="Nombre">
							<input type="email" id="defaultContactFormEmail" class="form-control mb-4"
								   placeholder="Correo electrónico">
							<label class="active">Asunto</label>
							<select class="browser-default custom-select mb-4">
								<option value="" disabled="">Tipo de mensaje</option>
								<option value="1" selected="">Feedback</option>
								<option value="2">Report de un problema</option>
								<option value="3">Petición</option>
								<option value="4">Comentarios</option>
								<option value="5">Dudas</option>
							</select>
							<div class="form-group">
						<textarea class="form-control rounded-0" id="exampleFormControlTextarea2" rows="3"
								  placeholder="Message"></textarea>
							</div>
							<div class="custom-control custom-checkbox mb-4">
								<input type="checkbox" class="custom-control-input" id="defaultContactFormCopy">
								<label class="custom-control-label" for="defaultContactFormCopy">Quiero una copia de
									este
									mensaje</label>
							</div>
							<button class="btn btn-info btn-block waves-effect waves-light" type="submit">
								aceptar
							</button>

						</form>
					</div>
				</div>

			</div>
			<div class="col-md-7 col-sm-12">

				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d425342.19154581043!2d-112.40524695037095!3d33.60567105256071!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x872b12ed50a179cb%3A0x8c69c7f8354a1bac!2sPhoenix%2C%20Arizona%2C%20EE.%20UU.!5e0!3m2!1ses-419!2ssv!4v1589331765685!5m2!1ses-419!2ssv"
						width="100%" height="500" frameborder="0" style="border:0;" allowfullscreen=""
						aria-hidden="false" tabindex="0"></iframe>

			</div>

		</div>

	</div>

</div>
