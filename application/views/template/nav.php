<!-- Navbar -->
<nav class="navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
	<div class="container-fluid">
		<!-- Brand -->
		<a class="navbar-brand" href="<?= site_url() ?>">
			<img src="<?= server_url() . $template->logo ?>" height="45" alt="Recursos IJM">
		</a>
		<!-- Collapse -->
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- Links -->
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- Left -->
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" href="<?= site_url() ?>">Inicio
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?= site_url() ?>contacto">Contacto</a>
				</li>
				<li class="nav-item ml-2">
					<form class="form-inline">
						<div class="md-form input-group my-0">
							<input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
							<div class="input-group-append">
								<button class="btn btn-md m-0 px-3 waves-effect waves-light" type="button">
									<i class="fas fa-search text-white"></i>
								</button>
							</div>
						</div>
					</form>
				</li>
			</ul>
		</div>

	</div>
</nav>
