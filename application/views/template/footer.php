<footer class="page-footer font-small primary-color-dark pt-4">
    <div class="container-fluid text-center text-md-left">
        <div class="row mt-4">
            <div class="col-md-3 mt-md-0 mt-3 text-center">
                <h5 >Contador de visitas este mes</h5>
                <h1 class="display-4">500</h1>
            </div>
            <hr class="clearfix w-100 d-md-none pb-3">
            <div class="col-md-4 mb-md-0 mb-3">
                <h5 class="">Links de interés</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#">Link 1</a>
                    </li>
                    <li>
                        <a href="#">Link 2</a>
                    </li>
                    <li>
                        <a href="#">Link 3</a>
                    </li>
                    <li>
                        <a href="#">Link 4</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-5 mb-4">
                <div class="mb-3 pb-3 flex-center">
                    <a class="fb-ic">
                        <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a class="ins-ic">
                        <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a class="tw-ic">
                        <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a class="gplus-ic">
                        <i class="fab fa-snapchat fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                    <a class="li-ic">
                        <i class="fab fa-youtube fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright text-center py-3">© <?php echo date('Y') ?> Todos los derechos reservados
        <a href="<?= site_url()?>">NGS Tecnology</a>
    </div>
</footer>

<!--Scripts Personalizados-->
<script type="text/javascript" src="<?= base_url() ?>public/js/template.js"></script>

<script type="text/javascript">
    // Animations initialization
    new WOW().init();
</script>
</body>
</html>
