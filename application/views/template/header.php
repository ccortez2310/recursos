<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="<?= server_url().$template->icono?>">

    <?php
    $e = array(
        'general' => true, //description
        'og' => true,
        'twitter'=> true,
        'robot'=> true
    );
    meta_tags($e, $headers);

    ?>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>public/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="<?= base_url() ?>public/css/mdb.min.css" rel="stylesheet">
	<!-- Video JS -->
	<link href="<?= base_url() ?>public/css/plugins/video-js/video-js.css" rel="stylesheet">
	<link href="<?= base_url() ?>public/css/plugins/video-js/estilos.css" rel="stylesheet">
	<!-- Your custom styles (optional) -->
    <link href="<?= base_url() ?>public/css/style.css" rel="stylesheet">

    <style type="text/css">
        @media (min-width: 800px) and (max-width: 850px) {
            .navbar:not(.top-nav-collapse) {
                background: #0E47A1!important;
            }
        }
    </style>

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?= base_url() ?>public/js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?= base_url() ?>public/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?= base_url() ?>public/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?= base_url() ?>public/js/mdb.min.js"></script>
	<!-- Video Js -->
	<script type="text/javascript" src="<?= base_url() ?>public/js/plugins/video-js/video.js"></script>

    <!-- FontAwesome -->
    <script type="text/javascript" src="<?= base_url() ?>public/js/fontawesome/js/all.min.js"></script>

</head>
<body>


