<main class="container d-flex justify-content-center mx-auto h-100 align-items-center">
	<!--Main layout-->

	<div class="mt-5 pt-5">

		<!-- First row -->
		<div class="row">
			<div class="col-md-12 text-center float-md-none mx-auto">
				<h1 class="error404 wow fadeIn" data-wow-delay="0.2s" style="font-weight: 500; visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.2s;">
					404
				</h1>
			</div>
		</div>
		<!-- /.First row -->

		<!-- Second row -->
		<div class="row mt-3">
			<div class="col-md-12 text-center mb-5">
				<h2 class="h2-responsive wow fadeIn mb-4" data-wow-delay="0.2s" style="font-weight: 500; visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.2s;">
					¡Oops! Página no encontrada
				</h2>
				<p class="wow fadeIn" data-wow-delay="0.4s" style="font-size: 1.25rem; visibility: visible; animation-name: fadeIn; animation-iteration-count: 1; animation-delay: 0.4s;">
					Haz click en el siguiente botón para volver al Inicio
				</p>
				<a href="<?= site_url()?>" class="btn btn-blue waves-effect waves-light">
					<i class="fas fa-home"></i>
				</a>

			</div>
		</div>
		<!-- /.Second row -->

	</div>

	<!--/.Main layout-->
</main>
