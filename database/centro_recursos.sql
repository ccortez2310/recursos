-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-05-2020 a las 22:39:38
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `centro_recursos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cabeceras`
--

CREATE TABLE `cabeceras` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `ruta` text NOT NULL,
  `descripcion` text NOT NULL,
  `keywords` text DEFAULT NULL,
  `portada` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cabeceras`
--

INSERT INTO `cabeceras` (`id`, `titulo`, `ruta`, `descripcion`, `keywords`, `portada`) VALUES
(1, 'Portal de Recursos del Cuidado del Trauma', 'inicio', 'En este portal encontrarás temas de interés social que te ayudarán a resolver problemas relacionados con factores psicologicos los cuales pueden ser capaz de llegar a afectar mucho la vida de las personas', 'portal cuidado del trauma, portal ijm', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `descripcion` text NOT NULL,
  `slug` text NOT NULL,
  `portada` text NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `categoria`, `descripcion`, `slug`, `portada`, `estado`) VALUES
(1, 'La Ansiedad', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores distinctio eum, inventore libero necessitatibus sint. Aliquam, beatae, deleniti distinctio error est harum incidunt ipsum nemo nostrum quibusdam quidem tempore vel?', 'la-ansiedad', 'uploads/categorias/la-ansiedad.jpg', 1),
(2, 'La Violencia Intrafamiliar', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores distinctio eum, inventore libero necessitatibus sint. Aliquam, beatae, deleniti distinctio error est harum incidunt ipsum nemo nostrum quibusdam quidem tempore vel?', 'la-violencia-intrafamiliar', 'uploads/categorias/la-violencia-intrafamiliar.jpg', 1),
(3, 'El Autoestima', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores distinctio eum, inventore libero necessitatibus sint. Aliquam, beatae, deleniti distinctio error est harum incidunt ipsum nemo nostrum quibusdam quidem tempore vel?', 'el-autoestima', 'uploads/categorias/el-autoestima.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plantilla`
--

CREATE TABLE `plantilla` (
  `id` int(11) NOT NULL,
  `logo` text NOT NULL,
  `icono` text NOT NULL,
  `email` varchar(60) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `redesSociales` text NOT NULL,
  `googleAnalytics` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `plantilla`
--

INSERT INTO `plantilla` (`id`, `logo`, `icono`, `email`, `telefono`, `redesSociales`, `googleAnalytics`) VALUES
(1, 'public/img/template/logo.png', 'public/img/template/icon.png', 'ijm@algo.com', '2211-1111', '[]', '[]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `podcasts`
--

CREATE TABLE `podcasts` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `slug` text NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `path` text NOT NULL,
  `poster` text DEFAULT NULL,
  `total_vistas` int(11) NOT NULL,
  `total_descargas` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `podcasts`
--

INSERT INTO `podcasts` (`id`, `titulo`, `descripcion`, `slug`, `id_categoria`, `path`, `poster`, `total_vistas`, `total_descargas`, `estado`, `fecha`) VALUES
(1, 'La Ansiedad es mala', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque, corporis ea ex, expedita impedit iste molestias obcaecati optio provident reiciendis repudiandae rerum sunt suscipit veritatis voluptatibus voluptatum. Aspernatur, quas.', 'la-ansiedad-es-mala', 1, 'uploads/podcasts/podcast-prueba.mp3', 'uploads/podcasts/posters/la-ansiedad.jpg', 2, 1, 1, '2020-05-06 00:00:00'),
(2, 'La Ansiedad no es buena', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque, corporis ea ex, expedita impedit iste molestias obcaecati optio provident reiciendis repudiandae rerum sunt suscipit veritatis voluptatibus voluptatum. Aspernatur, quas.', 'la-ansiedad-no-es-buena', 1, 'uploads/podcasts/podcast-prueba.mp3', 'uploads/podcasts/posters/la-ansiedad.jpg', 2, 1, 1, '2020-05-06 00:00:00'),
(3, 'La Ansiedad te puede matar', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda atque, corporis ea ex, expedita impedit iste molestias obcaecati optio provident reiciendis repudiandae rerum sunt suscipit veritatis voluptatibus voluptatum. Aspernatur, quas.', 'la-ansiedad-te-puede-matar', 1, 'uploads/podcasts/podcast-prueba.mp3', 'uploads/podcasts/posters/la-ansiedad.jpg', 2, 1, 1, '2020-05-06 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursos`
--

CREATE TABLE `recursos` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `ruta` text NOT NULL,
  `descripcion` text NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `url_recurso` text NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `totalVistas` int(11) DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `recursos`
--

INSERT INTO `recursos` (`id`, `titulo`, `ruta`, `descripcion`, `id_categoria`, `url_recurso`, `id_usuario`, `totalVistas`, `estado`, `fecha`) VALUES
(3, 'Video de Prueba ', 'video-de-prueba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 1, 'uploads/videos/ijm.mp4', 1, 0, 1, '2020-05-06'),
(4, 'Video de Prueba ', 'video-de-prueba-1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 1, 'uploads/videos/ijm.mp4', 1, 0, 1, '2020-05-06'),
(5, 'Video de Prueba ', 'video-de-prueba-2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua.', 1, 'uploads/videos/ijm.mp4', 1, 0, 1, '2020-05-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(60) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `pass` text DEFAULT NULL,
  `tipo_usuario` int(11) DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `pass`, `tipo_usuario`, `estado`, `fecha`) VALUES
(1, 'Carlos Cortez', 'admin@admin.com', 'admin', 1, 1, '2020-05-06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `titulo` text NOT NULL,
  `descripcion` text NOT NULL,
  `slug` text NOT NULL,
  `poster` text NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `path` text NOT NULL,
  `total_vistas` int(11) NOT NULL,
  `total_descargas` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `titulo`, `descripcion`, `slug`, `poster`, `id_categoria`, `path`, `total_vistas`, `total_descargas`, `estado`, `fecha`) VALUES
(1, '¿Cómo Reconocer la Ansiedad?', 'El video que usted observará a continuación tiene como objetivo, que comprenda en qué consiste la ansiedad y cómo reconocerla, identificando las respuestas de activación de su organismo ante situaciones que causen alarma a su cerebro.', 'como-reconocer-la-ansiedad', 'uploads/videos/posters/la-ansiedad.jpg', 1, 'uploads/videos/ijm.mp4', 3, 2, 1, '2020-05-06 00:00:00'),
(2, 'Detonantes y\r\nacciones de Mitigación', 'En este video se pretende que usted conozca e identifique los detonantes de la ansiedad y al identificarlos, pueda manejar las situaciones sin permitir que éstas evolucionen e intervengan para que la ansiedad no escale a un mayor nivel en su día a día.', 'detonantes-y-acciones-de-mitigacion', 'uploads/videos/posters/la-ansiedad.jpg', 1, 'uploads/videos/ijm.mp4', 2, 1, 1, '2020-05-06 00:00:00'),
(3, 'Estrategias y ejercicios prácticos', '¿Sabe usted si la ansiedad se puede prevenir? En el próximo video le brindaremos consejos y estrategias que le permitan identificar el momento en que se activa su ansiedad y pueda practicar una de estas estrategias, para prevención o control de todos los síntomas de ansiedad que se activan en su organismo.', 'estrategias-y-ejercicios-practicos', 'uploads/videos/posters/la-ansiedad.jpg', 1, 'uploads/videos/ijm.mp4', 3, 1, 1, '2020-05-06 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitaspaises`
--

CREATE TABLE `visitaspaises` (
  `id` int(11) NOT NULL,
  `pais` varchar(50) NOT NULL,
  `codigo` varchar(10) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `visitaspersonas`
--

CREATE TABLE `visitaspersonas` (
  `id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `pais` varchar(30) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cabeceras`
--
ALTER TABLE `cabeceras`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `plantilla`
--
ALTER TABLE `plantilla`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `podcasts`
--
ALTER TABLE `podcasts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `recursos`
--
ALTER TABLE `recursos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `recursos_usuarios_id_fk` (`id_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitaspaises`
--
ALTER TABLE `visitaspaises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `visitaspersonas`
--
ALTER TABLE `visitaspersonas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cabeceras`
--
ALTER TABLE `cabeceras`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `plantilla`
--
ALTER TABLE `plantilla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `podcasts`
--
ALTER TABLE `podcasts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `recursos`
--
ALTER TABLE `recursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `visitaspaises`
--
ALTER TABLE `visitaspaises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `visitaspersonas`
--
ALTER TABLE `visitaspersonas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `recursos`
--
ALTER TABLE `recursos`
  ADD CONSTRAINT `recursos_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id`),
  ADD CONSTRAINT `recursos_usuarios_id_fk` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
