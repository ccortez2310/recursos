$(document).ready(function ($) {

    let url = window.location.href;
    $('a[href="' + url + '"]').parent().addClass('active');

    let indice = url.split('/');
    let pagActual = indice[4];

    if (pagActual === "") {
        $(".navbar").removeClass("navbar-with-color");
    } else if (pagActual === "inicio") {
        $(".navbar").removeClass("navbar-with-color");
    } else {
        $(".navbar").addClass("navbar-with-color");
    }

});
